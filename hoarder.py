#!/usr/bin/python3

from __future__ import print_function

import sys
import os
import re
import glob
import signal
import requests

FOURCHANRE = re.compile('(\\/\\/\\w+\\.(?:4cdn|4chan).org/'
                        '\\w+\\/(\\d+\\.(?:jpg|png|gif|webm|mp4)))')
EIGHTCHANFSRE = re.compile('(\\/\\/media.8ch.net\\/file_store\\'
                           '/(\\w+\\.(?:jpg|png|gif|webm|mp4)))')
EIGHTCHANBORE = re.compile('(\\/\\/media.8ch.net\\/\\w+\\/src\\'
                           '/(\\d+(?:-|)\\d\\.(?:jpg|png|gif|webm|mp4)))')

def keyboard_interrupt(signal, frame):
    print("\nExiting")
    sys.exit(0)

signal.signal(signal.SIGINT, keyboard_interrupt)

def getFiles(argv):
    #If the user doesnt insert a url, exit the script
    try:
        dlink = argv[0]
    except IndexError:
        sys.exit("No URL received. Exiting")

    try:
        fdir = argv[1]
        if not os.path.exists(fdir):
            choice = input("The folder does not exist. Do you want to create it? [Y|n] ")
            while choice not in ("yes", "no", "Yes", "No", "Y", "N", "y", "n"):
                choice = input("The folder does not exist. Do you want to create it? [Y|n] ")
            if choice in ("no", "No", "N", "n"):
                sys.exit("Exiting")
            print("Creating directory", fdir)
            os.makedirs(fdir)
            os.chdir(fdir)
    except IndexError:
        choice = input("No folder received. Do you want to use the current folder? [Y|n] ")
        while choice not in ("yes", "no", "Yes", "No", "Y", "N", "y", "n"):
            choice = input("No folder received. Do you want to use the current folder? [Y|n] ")
        if choice in ("no", "No", "N", "n"):
            sys.exit("Exiting")

    if "-c" in argv or "--checkfiles" in argv:
        print("Checking files in the current directory")
        existingfiles = glob.glob("*")
    else:
        existingfiles = ""

    sess = requests.Session()
    req = sess.get(dlink, stream=True)

    try:
        connection = str(req.text)

        print ("Connection established")
        sys.stdout.write("Fetching")

        flist = ""
        if re.search('4chan.org', dlink) is not None:
            flist = re.findall(FOURCHANRE, connection)
        elif re.search('8ch.net', dlink) is not None:
            flist = re.findall(EIGHTCHANFSRE, connection) + re.findall(EIGHTCHANBORE, connection)

        if flist is not "":
            for link, img in flist:
                sys.stdout.write(".")   #Progress bar
                sys.stdout.flush()     #Progress bar
                furl = 'https:' + link
                if img not in existingfiles:
                    filereq = sess.get(furl, stream=True)
                    with open(img, 'wb') as mediafile:
                        for data in filereq.iter_content(chunk_size=1024):
                            if data:
                                mediafile.write(data)
            print("Done")

    except ValueError:
        print(dlink, "is not a url")

def helpfunc():
    print("Usage: hoarder [ options ... ] [URL] [output folder]")
    print("       where options include:")
    print("")
    print("  -c or --checkfiles       Check if the files already exist in the folder")
    print("  -h or --help             Print this message")

def main(argv):
    if "-h" in argv or "--help" in argv:
        helpfunc()
    else:
        getFiles(argv)

if __name__ == "__main__":
    main(sys.argv[1:])
